const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.sendFile(__dirname+'/index.html'))
app.get('/stream1', (req, res) => res.sendFile('/tmp/stream1.jpg'))


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
